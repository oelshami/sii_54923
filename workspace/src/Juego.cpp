#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

int main(){
	
	int a,b,c,d,e;
	a=fork();
	if(a==0){
		if(a==-1){
			perror("fork");
			return 1;
		}
		execlp("./logger","./logger",NULL);
			perror("execlp");
			return 1;
	}
	b=fork();
	if(b==0){
		if(b==-1){
			perror("fork");
			return 1;
		}
		sleep(0.5);
		execlp("./servidor","./servidor",NULL);
		perror("execlp");
		return 1;
	}
	c=fork();
	if(c==0){
		if(c==-1){
			perror("fork");
			return 1;
		}
		sleep(1);
		execlp("./cliente","./cliente",NULL);
		perror("execlp");
		return 1;
	}
	d=fork();
	if(d==0){
		if(d==-1){
			perror("fork");
			return 1;
		}
		sleep(1.5);
		execlp("./bot","./bot","/tmp/Bot.txt","25000",NULL);
		perror("execlp");
		return 1;
	}
	e=fork();
	if(e==0){
		if(e==-1){
			perror("fork");
			return 1;
		}
		sleep(2);
		execlp("./bot","./bot","/tmp/Bot2.txt","100000",NULL);
		perror("execlp");
		return 1;
	}	
	waitpid(b,NULL,0);
	kill(c,3);
	kill(d,3);
	kill(e,3);
	return 0;
}
