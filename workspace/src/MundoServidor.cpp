//MI numero de matricula es 54923
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	sprintf(CliServidorFIFO,"/tmp/CliServidorFIFO");
	unlink(CliServidorFIFO);
	int mkCliSer=mkfifo(CliServidorFIFO,0777);
	if(mkCliSer==-1){
		perror("mkfifo CliServidorFIFO");
		exit(1);
	}
	fdCliSer=open(CliServidorFIFO,O_RDONLY);
     while (1) {
	 	usleep(10);
		char cad[100];
		read(fdCliSer, cad, sizeof(cad));
		unsigned char key;
		sscanf(cad,"%c",&key);
		 	switch(key)
			{
			//	case 'a':jugador1.velocidad.x=-1;break;
			//	case 'd':jugador1.velocidad.x=1;break;
				case 's':jugador1.velocidad.y=-4; break;
				case 'w':jugador1.velocidad.y= 4; break;
				case 'l':jugador2.velocidad.y=-4;break;
				case 'o':jugador2.velocidad.y=4;break;
				case 'r':jugador1.RestaurarTam(); jugador2.RestaurarTam();break;
				case 'e': case 'E': jugador2.ReducirTam(0.2);break;
				case 'p': case 'P': jugador1.ReducirTam(0.2);break;
				case 'z': esfera.velocidad.y*=1.2; esfera.velocidad.x*=1.2;break;
				case 'x': esfera.velocidad.y*=1/1.2; esfera.velocidad.x*=1/1.2;break;
			}
  }
}

CMundo::~CMundo()
{	
	close(fdLogger);
	unlink(ServidorClienteFIFO);
	close(fdCliSer);
	unlink(CliServidorFIFO);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char m1[100]="Jugador 2 marca 1 punto, lleva un total de ";
		char puntos2C[2];
		sprintf(puntos2C, "%d", puntos2);
		char m2[100]=" puntos.\n";
		char mensaje2[100];
		strcat(m1,puntos2C);
		strcat(m1,m2);
		int w1=write(fdLogger,m1,sizeof(m1));
		if(w1==-1){
			perror("write Logger");
			return;
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char m3[100]="Jugador 1 marca 1 punto, lleva un total de ";
		char puntos1C[2];
		sprintf(puntos1C, "%d", puntos1);
		char m4[100]=" puntos.\n";
		char mensaje1[100];
		strcat(m3,puntos1C);
		strcat(m3,m4);
		int w2=write(fdLogger,m3,sizeof(m4));
		if(w2==-1){
			perror("write Logger");
			return;
		}
	}
	
	//Datos que manda el servidor al cliente
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,
	jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,
	jugador2.y2, puntos1, puntos2);

	write(fdSerCliente,cad,sizeof(cad));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void terminar(int s){
	if(s==12){
		printf("./servidor termina correctamente (0)\n");
		exit(0);
		}
	else{
		printf("./servidor termina por la señal %d\n",s);
		exit(1);
		}
		
}

void CMundo::Init()
{	
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	struct sigaction acc;
	acc.sa_handler=terminar;
	acc.sa_flags=0;
	sigemptyset(&acc.sa_mask);
	sigaction(SIGINT,&acc,NULL);
	sigaction(SIGTERM,&acc,NULL);
	sigaction(SIGPIPE,&acc,NULL);
	sigaction(SIGUSR2,&acc,NULL);	
	
	sprintf(loggerFIFO,"/tmp/loggerFIFO");
	sprintf(ServidorClienteFIFO,"/tmp/SerClienteFIFO");
	
	fdLogger=open(loggerFIFO,O_WRONLY);
	if(fdLogger==-1){
		perror("open loggerFIFO");
		return;
	}
	
	unlink(ServidorClienteFIFO);
	int mkC=mkfifo(ServidorClienteFIFO,0777);
	if(mkC==-1){
		perror("mkfifo clienteFIFO");
		return;
	}
	
	fdSerCliente=open(ServidorClienteFIFO,O_WRONLY);
	if(fdSerCliente==-1){
		perror("open clienteFIFO");
		return;
	}
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

