//MI numero de matricula es 54923
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{	
	int mun1=munmap(pdatosJ1, sizeof(DatosMemCompartida));
	if(mun1==-1){
		perror("munmap Bot1");
		return;
	}
	int mun2=munmap(pdatosJ2, sizeof(DatosMemCompartida));
	if(mun2==-1){
		perror("munmap Bot2");
		return;
	}
	//cerrar descriptor de fichero de la tubería con el Servidor
	close(fdCliServidor);
	close(fdSerCliente);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	////Datos recibidos del servidor
	
	char cad[200];
	int r=read(fdSerCliente,cad,sizeof(cad));
	if(r==-1){
		perror("read fdSerCliente");
		exit(1);
	}
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,
	&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,
	&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
	//printf("%s\n",cad);
	
	/////Datos compartidos con el bot
	pdatosJ1->esfera = esfera;
	pdatosJ1->raqueta1 = jugador1;
	switch(pdatosJ1->accion){
		case 1:
			OnKeyboardDown('w', -1, -1);
		break;
		case -1:
			OnKeyboardDown('s', -1, -1);
		break;
	}
	
	
	//Jugador 2 Bot
	pdatosJ2->esfera = esfera;
	pdatosJ2->raqueta1 = jugador2;
	switch(pdatosJ2->accion){
		case 1:
			OnKeyboardDown('o', -1, -1);
		break;
		case -1:
			OnKeyboardDown('l', -1, -1);
		break;
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	static char cad[100];
	sprintf(cad,"%c",key);
	write(fdCliServidor,cad,sizeof(cad));
}

void CMundo::Init()
{	

	//Comunicación Cliente-Servidor
	sprintf(CliServidorFIFO,"/tmp/CliServidorFIFO");	
	fdCliServidor=open(CliServidorFIFO,O_WRONLY);
	if(fdCliServidor==-1){
		perror("open fdCliServidor");
		exit(1);
	}
	
	//Iniciar comunicación con servidor mediante FIFO
	sprintf(SerClienteFIFO,"/tmp/SerClienteFIFO");
	fdSerCliente=open(SerClienteFIFO,O_RDONLY);
	if(fdSerCliente==-1){
		perror("open SerClienteFIFO");
		return;
	}
	//Proyección en memoria para la comunicación con bo
	fdBot1 = open("/tmp/Bot.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);
	if(fdBot1==-1){
		perror("open Bot.txt");
		return;
	}
	fdBot2 = open("/tmp/Bot2.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);
	if(fdBot2==-1){
		perror("open Bot2.txt");
		return;
	}
	
	/////
	int w3=write(fdBot1, &datosConfig, sizeof(DatosMemCompartida));
	if(w3==-1){
		perror("write Bot1");
		return;
	}
	struct stat bstat;
	fstat(fdBot1, &bstat);
	
	int w4=write(fdBot2, &datosConfig, sizeof(DatosMemCompartida));
	if(w4==-1){
		perror("write Bot2");
		return;
	}
	struct stat bstat2;
	fstat(fdBot2, &bstat2);
	
	pdatosJ1 = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fdBot1, 0));
	if(pdatosJ1==MAP_FAILED){
		perror("mmap Bot1");
		return;
	}
	pdatosJ2 = static_cast<DatosMemCompartida*>(mmap(NULL, bstat2.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fdBot2, 0));
	if(pdatosJ1==MAP_FAILED){
		perror("mmap Bot1");
		return;
	}
	
	close(fdBot1);
	close(fdBot2);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
