// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1 += velocidad.x *t;
	x2 += velocidad.x *t;
	y1 += velocidad.y *t;
	y2 += velocidad.y *t;
}

void Raqueta::ReducirTam(float menos){
	if(y2-y1 > 0.5)
		y2 -= menos;
}

void Raqueta::RestaurarTam(){
	y2 = y1 +2;
}
