//#include "Logger.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main(void){
	char fifo[]="/tmp/loggerFIFO";
	unlink(fifo);
	int mk=mkfifo(fifo, 0777);
	if(mk==-1){
		perror("mkfifo"); 
		return 1;
	} 
	int n, fd =open(fifo,O_RDONLY); 
	if(fd ==-1){
		perror("open"); 
		return 1;
	}
	char datos[100];
	while(int r=read(fd, datos,sizeof(datos))!=NULL){
		if(r==-1){
			perror("read");
			return 1;
		}
		printf("%s",datos);
	}
	close(fd);
	unlink(fifo);
	return 0;
}
