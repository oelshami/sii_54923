#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "DatosMemCompartida.h"

int main(int argc, char* argv[]){

	int fdBot= open(argv[1], O_RDWR),espera=atoi(argv[2]);
	if(fdBot==-1){
		perror("open");
		return 1;
	}
	DatosMemCompartida *pDatos = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fdBot, 0));
	if(pDatos==MAP_FAILED){
		perror("mmap");
		return 1;	
	}
	close(fdBot);
	
	while(1){
		usleep(espera);
		if((pDatos->raqueta1.y1+pDatos->raqueta1.y2)/2 - pDatos->esfera.centro.y < 0){
			pDatos->accion =1;
		} else if((pDatos->raqueta1.y1+pDatos->raqueta1.y2)/2 - pDatos->esfera.centro.y > 0){
			pDatos->accion=-1;	
		} else{
			pDatos->accion=0;
		}
	}
	int mun=munmap(pDatos, sizeof(DatosMemCompartida));
	if(mun==-1){
		perror("munmap");
		return 1;
	}	
	return 0;
}
