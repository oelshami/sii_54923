#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

class Logger{
	
	//char *fifo = "loggerFIFO";
	int fd;
	public:
		void openFIFO();
		Logger();
		void closeFIFO();
		void readFIFO(char* datos, int tamDatos);
};


